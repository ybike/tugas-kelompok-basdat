from django import forms
from django.forms import ModelForm
from . import models

date_attrs = {
    'type':'date',
    'class':'form-control'
}
class_attrs = {
    'class':'form-control'
}

class FormRegis(forms.Form):
    ktp = forms.IntegerField(
        label="No. KTP",
        max_value=99999999999999999999,
        required=True,
        widget=forms.NumberInput(attrs=class_attrs)
    )
    
    email = forms.EmailField(
        label="Email",
        max_length=50,
        required=True,
        widget=forms.EmailInput(attrs=class_attrs)
    )

    nama = forms.CharField(
        label="Nama Lengkap",
        max_length=50,
        required=True,
        widget=forms.TextInput(attrs=class_attrs)
    )

    alamat = forms.CharField(
        label="Alamat",
        widget=forms.Textarea(attrs=class_attrs)
    )

    tgl_lahir = forms.DateField(
        label="Tanggal Lahir",
        required=True,
        widget=forms.DateInput(attrs=date_attrs)
    )

    no_telp = forms.IntegerField(
        label="Nomor Telepon",
        max_value=99999999999999999999,
        widget=forms.NumberInput(attrs=class_attrs)
    )

    role = forms.CharField(widget=forms.HiddenInput())
