# Generated by Django 2.1.1 on 2019-05-06 04:59

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=50)),
                ('ktp', models.IntegerField(max_length=20)),
                ('tgl_lahir', models.DateField()),
                ('alamat', models.TextField()),
                ('no_telp', models.IntegerField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='BikeSharing.User')),
                ('isAdmin', models.BooleanField(default=True, editable=False)),
            ],
            bases=('BikeSharing.user',),
        ),
        migrations.CreateModel(
            name='Anggota',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='BikeSharing.User')),
                ('saldo', models.FloatField()),
                ('points', models.IntegerField(default=0)),
            ],
            bases=('BikeSharing.user',),
        ),
        migrations.CreateModel(
            name='Petugas',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='BikeSharing.User')),
                ('gaji', models.FloatField(default=30000)),
            ],
            bases=('BikeSharing.user',),
        ),
    ]
