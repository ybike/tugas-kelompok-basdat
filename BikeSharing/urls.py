from django.urls import path
from . import views

app_name = "index"
urlpatterns = [
    path('adminRegis/', views.adminRegis),
    path('petugasRegis/', views.petugasRegis),
    path('anggotaRegis/', views.anggotaRegis),
    path('',views.index,name ='index'),
    
]
