from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from django.db import connection
from otentikasi.views import login
import json

from .forms import FormRegis

def use_cursor(operation, query):
    result = None
    with connection.cursor() as cursor:
        cursor.execute(query)
        
        if operation == 'getAll':
            result = cursor.fetchall()
        elif operation == 'getOne':
            result = cursor.fetchone()
        else:
            pass
    return result
            

def adminRegis(request):
    return create_account_form(request, 'admin', {})
    
def petugasRegis(request):
    return create_account_form(request, 'petugas', {})

def anggotaRegis(request):
    return create_account_form(request, 'anggota', {})

def index(request):
    return render(request,"index.html")

def create_account(request, role):
    form = FormRegis(request.POST or None)
    
##    if form.is_valid():
    nama = request.POST.get('nama')
    email = request.POST.get('email')
    ktp = request.POST.get('ktp')
    tgl_lahir = request.POST.get('tgl_lahir')
    alamat = request.POST.get('alamat')
    no_telp = request.POST.get('no_telp')
    role = role
    
    #checking availability
    queryCheck = "SELECT ktp, email, nama FROM PERSON "\
                 +" WHERE ktp='"+ktp+"' OR email='"+email+"'"
    hasTaken = use_cursor('getOne', queryCheck)
    
    if(hasTaken):
        invalid_reg = "Email atau ktp sudah pernah terdaftar!"
        request.method = 'GET'
        return create_account_form(request, role, {"invalid_reg": invalid_reg})
    
    use_cursor('insert', "INSERT INTO PERSON VALUES ('"+
                       ktp+"', '"+email+"', '"+nama+"', '"+alamat+"', '"+
                       tgl_lahir+"', "+no_telp+")")
    
    if role == 'petugas':
        query = "INSERT INTO PETUGAS VALUES ("+ktp+", 30000)"
    elif role == 'admin':
        query = "INSERT INTO ADMIN VALUES ("+ktp+")"
    elif role == 'anggota':
        anggota_id_list = use_cursor('getAll', "SELECT no_kartu FROM ANGGOTA")
        newId = 1000000000
        for aTuple in anggota_id_list:
            if str(newId) != aTuple[0]:
                break
            newId += 1
            
        query = "INSERT INTO ANGGOTA VALUES ("+str(newId)+", 0, 0, "+ktp+")"
    use_cursor('insert', query)

    request.session['user'] = use_cursor('getOne', queryCheck)
    request.session['role'] = role
    print(request.session['role'])
    print(request.session['user'])
    return redirect('/')

def create_account_form(request, role, response):
    if request.method == 'POST':
        return create_account(request, role)
    
    form = FormRegis(request.POST or None)
    
    response["formRegis"] = form
    return render(request,"regis_form.html",response)
