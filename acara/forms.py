from django import forms
from django.db import connection

cursor = connection.cursor()


query_id_nama_stasiun = "SELECT id_stasiun, nama FROM STASIUN;"
cursor.execute(query_id_nama_stasiun)

id_nama_stasiun_tuple = cursor.fetchall()
id_nama_stasiun = []
PILIHAN_STASIUN = [
    ("----Pilih stasiun----", "----Pilih stasiun----"),
    ("(Bukan acara stasiun)", "(Bukan acara stasiun)")
]


for tuples in id_nama_stasiun_tuple:
    id_stasiun = tuples[0]
    nama_stasiun = tuples[1]
    id_nama_stasiun.append(id_stasiun + " - " + nama_stasiun)


for st in id_nama_stasiun:
    id_nama_id_nama = (st, st)
    PILIHAN_STASIUN.append(id_nama_id_nama)


class FormAcara(forms.Form):
    PILIHAN_GRATIS = [
        ('Ya', 'Ya'),
        ('Tidak', 'Tidak')
    ]

    YEARS = [year for year in range(2000, 2090)]

    date_attrs = {
        'type': 'date'
    }

    color_attribute = {
        'style': 'color:black;'
    }

    judul = forms.CharField(
        label='Judul',
        required=True,
        widget=forms.TextInput(attrs=color_attribute)
    )

    deskripsi = forms.CharField(
        label='Deskripsi',
        required=True,
        widget=forms.TextInput(attrs=color_attribute)
    )

    gratis = forms.ChoiceField(
        choices=PILIHAN_GRATIS,
        required=True,
    )

    tanggal_mulai = forms.DateField(
        label='Tanggal mulai:',
        required=True,
        widget=forms.DateInput(attrs=date_attrs)
    )

    tanggal_selesai = forms.DateField(
        label='Tanggal selesai',
        required=True,
        widget=forms.DateInput(attrs=date_attrs)
    )

    stasiun = forms.ChoiceField(
        required=True,
        choices=PILIHAN_STASIUN
    )
