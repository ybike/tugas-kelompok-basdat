from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'acara'
urlpatterns = [
    path('update', views.update_acara, name='update_acara'),
    path('create', views.create_acara, name='create_acara'),
    path('delete', views.delete_acara, name='delete_acara'),
    path('update/', views.update_acara, name='update_acara'),
    path('create/', views.create_acara, name='create_acara'),
    path('delete/', views.delete_acara, name='delete_acara'),
    path('', views.list_acara, name='acara'),
    url(r'^delete/(?P<acara_id>\d+)$', views.delete_acara, name='delete_acara'),
    url(r'^update/(?P<acara_id>\d+)$', views.update_acara, name='update_acara')
]
