from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.db import connection
from .forms import FormAcara


def execute_query(query):
    with connection.cursor() as cursor:
        try:
            cursor.execute(query)
            hasil = cursor.fetchall()
            print("HASIL: ")
            print(hasil)
            connection.close()
            cursor.close()
            return hasil
        except Exception:
            return


def list_acara(request):

    sql_query = "SELECT * FROM ACARA"

    acara_objects = execute_query(sql_query)

    response = {
        "acara_objects": acara_objects
    }

    return render(request, 'acara_list.html', response)


def create_acara(request):
    form = FormAcara(request.POST or None)
    response = {}

    query_get_all = "SELECT id_acara FROM ACARA"

    list_id_kepake_masih_tuple = execute_query(query_get_all)

    list_id_kepake = []
    for tuples in list_id_kepake_masih_tuple:
        list_id_kepake.append(tuples[0])

    id_terakhir = int(list_id_kepake[len(list_id_kepake) - 1])

    if request.method == 'POST' and form.is_valid():
        judul = request.POST.get('judul')
        deskripsi = request.POST.get('deskripsi')
        tanggal_mulai = request.POST.get('tanggal_mulai')
        tanggal_selesai = request.POST.get('tanggal_selesai')
        stasiun = request.POST.get('stasiun')
        gratis = request.POST.get('gratis')

        if stasiun == '----Pilih stasiun----' or stasiun == '(Bukan acara stasiun)':
            stasiun == '-'

        if gratis == 'Ya':      # di tabel cuma bisa 't' sama 'f'
            gratis = 't'
        elif gratis == 'Tidak':
            gratis = 'f'

        # get id terakhir trus diincrement
        id_terakhir = int(list_id_kepake[len(list_id_kepake) - 1])
        id_acara = id_terakhir + 1

        while str(id_acara) in list_id_kepake:
            id_acara += 1

        sql_query = "INSERT INTO ACARA VALUES("
        sql_query += "'" + str(id_acara) + "',"
        sql_query += "'" + judul + "',"
        sql_query += "'" + deskripsi + "',"
        sql_query += "'" + tanggal_mulai + "',"
        sql_query += "'" + tanggal_selesai + "',"

        sql_query += "'" + gratis + "',"
        sql_query += "'" + stasiun + "')"
        #print("QUERY: " + sql_query)
        execute_query(sql_query)

        response = {
            "form": form,
            "stasiun": stasiun
        }

        return redirect('/acara/')

    response = {"form": form}

    return render(request, 'acara_create.html', response)


def update_acara(request, acara_id):

    query_data_row_yg_mau_diupdate = "SELECT * FROM ACARA WHERE id_acara = '"
    query_data_row_yg_mau_diupdate += acara_id + "';"

    data_row_yg_mau_diupdate = execute_query(query_data_row_yg_mau_diupdate)[0]

    attributes = {
        #'form': form,
        #'id_acara': data_row_yg_mau_diupdate[0],
        'judul': data_row_yg_mau_diupdate[1],
        'deskripsi': data_row_yg_mau_diupdate[2],
        'tanggal_mulai': data_row_yg_mau_diupdate[3],
        'tanggal_selesai': data_row_yg_mau_diupdate[4],
        'gratis': data_row_yg_mau_diupdate[5],
        'stasiun': data_row_yg_mau_diupdate[6]
    }
    #form = FormAcara(request.POST or None)
    form = FormAcara(initial=attributes)
    if request.method == 'POST':
        print("FORM VALID")
        judul_baru = request.POST.get('judul')
        deskripsi_baru = request.POST.get('deskripsi')
        tanggal_mulai_baru = request.POST.get('tanggal_mulai')
        tanggal_selesai_baru = request.POST.get('tanggal_selesai')
        stasiun_baru = request.POST.get('stasiun')
        gratis_baru = request.POST.get('gratis')

        if gratis_baru == 'Ya':
            gratis_baru = 't'
        elif gratis_baru == 'Tidak':
            gratis_baru = 'f'

        update_query = "UPDATE ACARA SET judul = '"
        update_query += judul_baru + "', deskripsi = '"
        update_query += deskripsi_baru + "', tgl_mulai = '"
        update_query += tanggal_mulai_baru + "', tgl_akhir = '"
        update_query += tanggal_selesai_baru + "', is_free = '"
        update_query += gratis_baru + "' "
        update_query += "WHERE id_acara = '" + acara_id + "';"

        print(update_query)
        execute_query(update_query)

        return redirect('/acara/')

    attributes['form'] = form
    return render(request, 'acara_update.html', attributes)


def delete_acara(request, acara_id):
    delete_query = "DELETE FROM ACARA WHERE id_acara = '" + acara_id + "'"
    execute_query(delete_query)

    return redirect('/acara/')
