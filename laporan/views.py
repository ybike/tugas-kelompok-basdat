from django.shortcuts import render
from django.db import connection

# Create your views here.
def use_cursor(operation, query):
    result = None
    with connection.cursor() as cursor:
        cursor.execute(query)
        
        if operation == 'getAll':
            result = cursor.fetchall()
        elif operation == 'getOne':
            result = cursor.fetchone()
        else:
            pass
    return result

def show_laporan (request):
    laporan = make_laporan()
    response = { "laporan" : laporan }
    return render(request, "laporan_show.html", response)

def make_laporan():
    laporanAkhir = []

    query = "SELECT id_laporan, no_kartu_anggota, datetime_pinjam, "+\
            "denda FROM PEMINJAMAN P NATURAL JOIN LAPORAN L"
    laporanPeminjamanTuple = use_cursor('getAll', query)

    urutan = 1
    for aTuple in laporanPeminjamanTuple:
        query = "SELECT nama FROM PERSON P JOIN ANGGOTA A "+\
                "ON P.ktp = A.ktp WHERE A.no_kartu = '"+ aTuple[1] + "'"
        namaAnggota = use_cursor('getOne', query)
        satuAnggota = str(aTuple[1]) + " - " + namaAnggota[0]
        
        laporanAkhir.append((urutan, aTuple[0], aTuple[2], satuAnggota,
                             aTuple[3]))
        urutan += 1
    return laporanAkhir
