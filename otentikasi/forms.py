from django import forms
from django.forms import ModelForm
from . import models

class FormLogin(forms.Form):
    ktp = forms.IntegerField(
        label="No. KTP",
        max_value=99999999999999999999,
        required=True,
    )

    email = forms.EmailField(
        label="Email",
        max_length=50,
        required=True
    )
