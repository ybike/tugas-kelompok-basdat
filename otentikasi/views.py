from django.shortcuts import render, redirect

from django.db import connection

from .forms import FormLogin

# Create your views here.

def use_cursor(operation, query):
    result = None
    with connection.cursor() as cursor:
        cursor.execute(query)
        
        if operation == 'getAll':
            result = cursor.fetchall()
        elif operation == 'getOne':
            result = cursor.fetchone()
        else:
            pass
    return result

def login(request):
    form = FormLogin(request.POST or None)
    print("masok")
    response = {}
    if request.method == 'POST':
        email = request.POST.get('email')
        ktp = request.POST.get('ktp')

        query = "SELECT ktp, email, nama FROM PERSON "+\
                "WHERE ktp='"+ktp+"' and email='"+email+"'"
        user = use_cursor('getOne', query)
        print(user)
        
        if(not user):
            response["noAcc_warn"] = "Akun tidak terdaftar!"
        else:
            request.session['user'] = user
            print(request.session['user'])
            
            if(use_cursor('getOne', "SELECT * FROM ADMIN WHERE ktp='"+ktp+"'")):
                request.session['role'] = 'admin'
            elif(use_cursor('getOne', "SELECT * FROM PETUGAS WHERE ktp='"+ktp+"'")):
                request.session['role'] = 'petugas'
            elif(use_cursor('getOne', "SELECT * FROM ANGGOTA WHERE ktp='"+ktp+"'")):
                request.session['role'] = 'anggota'
            
            return redirect("/")
        

    response["formLogin"] = form
    return render(request, 'login.html', response)

def logout(request):
    request.session.flush()
    return redirect("/")
