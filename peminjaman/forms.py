from django import forms
from django.db import connection

cursor = connection.cursor()

query_sepeda_stasiun = "SELECT STASIUN.nama, SEPEDA.nomor, STASIUN.id_stasiun "+\
                       "FROM STASIUN, SEPEDA " +\
                       "WHERE SEPEDA.id_stasiun = STASIUN.id_stasiun"
cursor.execute(query_sepeda_stasiun)
sepeda_stasiun_tuple = cursor.fetchall()
sepeda_stasiun = []
sepeda_idStasiun = []

PILIHAN_SEPEDA_STASIUN = []

for tuples in sepeda_stasiun_tuple:
    nama_st = tuples[0]
    nomor_sepeda = tuples[1]
    id_st = tuples[2]

    choice = nomor_sepeda + " - " + nama_st + " - " + id_st
    choice_tuple = (choice, choice)
    PILIHAN_SEPEDA_STASIUN.append(choice_tuple)
    #sepeda_idStasiun.append(id_st + " - " + nomor_sepeda)
    #sepeda_stasiun.append(nama_st + " - " + nomor_sepeda)
"""
urutan = 0
for namaSt in sepeda_stasiun:
    info = nomor_sepeda + "- Stasiun " +  
    nama_nomor = (sepeda_idStasiun[urutan], namaSt)
    PILIHAN_SEPEDA_STASIUN.append(nama_nomor)
    urutan += 1
"""

class FormPeminjaman(forms.Form):

    sepeda_stasiun = forms.ChoiceField(
        required=True,
        choices=PILIHAN_SEPEDA_STASIUN
    )
