from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'peminjaman'
urlpatterns = [
    path('update', views.update_peminjaman, name='update_peminjaman'),
    path('create', views.create_peminjaman, name='create_peminjaman'),
    path('update/', views.update_peminjaman, name='update_peminjaman'),
    path('create/', views.create_peminjaman, name='create_peminjaman'),
    path('', views.list_peminjaman, name='peminjaman'),
    path('history/', views.history_peminjaman, name='peminjaman'),
    url(r'^update/(?P<no_kartu_anggota>\d+)$', views.update_peminjaman, name='update_peminjaman')
]
