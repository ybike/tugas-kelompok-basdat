from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.db import connection
from .forms import FormPeminjaman
import datetime
import random

def execute_query(query):
    with connection.cursor() as cursor:
        try:
            cursor.execute(query)
            hasil = cursor.fetchall()
            print("HASIL: ")
            print(hasil)
            connection.close()
            cursor.close()
            return hasil 
        except Exception:  # insert delete update kan ga return apa2
            return


def history_peminjaman(request):
    sql_query = "SELECT * FROM PEMINJAMAN"
    peminjaman_objects = execute_query(sql_query)
    print("PEMINJAMAN OBJECTS:")
    print(peminjaman_objects)
    response = {
        "peminjaman_objects": peminjaman_objects
    }

    return render(request, 'peminjaman_show.html', response)


def create_peminjaman(request):
    form = FormPeminjaman(request.POST or None)
    response = {}

    # NO KARTU ANGGOTANYA MASIH RANDOM! NANTI HRS TAU GMN CARANYA
    # AMBIL NO KARTU DARI YG LOGIN!
    if request.method == 'POST':
        sepeda_stasiun = request.POST.get('sepeda_stasiun')
        sepeda_stasiun_split = sepeda_stasiun.split(" - ")
        no_sepeda = sepeda_stasiun_split[0]
        id_stasiun = sepeda_stasiun_split[2]

        datetime_pinjam = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        no_kartu_anggota_tuple = execute_query("SELECT no_kartu FROM ANGGOTA")
        no_kartu_anggota = []

        for tuples in no_kartu_anggota_tuple:
            no_kartu_anggota.append(tuples[0])

        print("NO KARTU ANGGOTA: ")
        print(no_kartu_anggota)
        # print("DATETIME PINJAM: ")
        # print(datetime_pinjam)
        random_index = random.randint(0, len(no_kartu_anggota) + 1)
        no_kartu_anggota_random = no_kartu_anggota[random_index]

        sql_query = "INSERT INTO PEMINJAMAN VALUES("
        sql_query += "'" + no_kartu_anggota_random + "',"
        sql_query += "'" + datetime_pinjam + "', "
        sql_query += "NULL, '0', '0', "  # tgl kembali, biaya, denda
        sql_query += "'" + str(no_sepeda) + "',"
        sql_query += "'" + str(id_stasiun) + "')"

        execute_query(sql_query)
        print(sql_query)

        response = {
            "form": form,
        }

        return redirect('/peminjaman/')

    response = {"form": form}

    return render(request, 'peminjaman_create.html', response)


def update_peminjaman(request, no_kartu_anggota):

    # sql_query = "SELECT * FROM PEMINJAMAN"
    # peminjaman_objects = execute_query(sql_query)

    # response = {
    #     "peminjaman_objects": peminjaman_objects
    # }

    # return render(request, 'peminjaman_berjalan.html', response)

    query_data_row_yg_mau_diupdate = "SELECT * FROM PEMINJAMAN WHERE no_kartu_anggota = '"
    query_data_row_yg_mau_diupdate += no_kartu_anggota + "';"

    data_row_yg_mau_diupdate = execute_query(query_data_row_yg_mau_diupdate)[0]

    form = FormPeminjaman(request.POST or None)

    if request.method == 'POST':
        query_data_row_yg_mau_diupdate = "SELECT * FROM PEMINJAMAN WHERE no_kartu_anggota = '"
        query_data_row_yg_mau_diupdate += no_kartu_anggota + "';"

        data_row_yg_mau_diupdate = execute_query(query_data_row_yg_mau_diupdate)[0]
        datetime_pinjam = data_row_yg_mau_diupdate[1]
        datetime_kembali = datetime.datetime.now()

        elapsedTime = datetime_kembali - datetime_pinjam
        bayaran = (elapsedTime.total_seconds()//3600+1) * 1000

        update_query = "UPDATE PEMINJAMAN SET datetime_kembali = '"
        update_query += str(datetime_kembali) + "', biaya = " + str(bayaran)
        update_query += " WHERE no_kartu_anggota = '" + no_kartu_anggota + "';"

        print(update_query)
        execute_query(update_query)

        return redirect('/peminjaman/')

    attributes = {
        'form': form,
        'no_kartu_anggota': data_row_yg_mau_diupdate[0],
        'datetime_pinjam': data_row_yg_mau_diupdate[1],
        'datetime_kembali': data_row_yg_mau_diupdate[2],
        'biaya': data_row_yg_mau_diupdate[3],
        'denda': data_row_yg_mau_diupdate[4],
        'nomor_sepeda': data_row_yg_mau_diupdate[5],
        'id_stasiun': data_row_yg_mau_diupdate[6]
    }

    return render(request, 'peminjaman_berjalan.html', attributes)

def list_peminjaman(request):

    sql_query = "SELECT * FROM PEMINJAMAN"
    peminjaman_objects = execute_query(sql_query)

    response = {
        "peminjaman_objects": peminjaman_objects
    }

    return render(request, 'peminjaman_berjalan.html', response)

