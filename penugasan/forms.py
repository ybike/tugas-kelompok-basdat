from django import forms
from django.db import connection
# http://www.learningaboutelectronics.com/Articles/How-to-create-a-drop-down-list-in-a-Django-form.php
cursor = connection.cursor()

# Ngambil semua no ktp dr petugas
query_no_ktp_petugas = "SELECT ktp FROM PETUGAS;"
cursor.execute(query_no_ktp_petugas)

no_ktp_petugas_tuple = cursor.fetchall()
no_ktp_petugas = []
PILIHAN_PETUGAS = []

for tuples in no_ktp_petugas_tuple:
    no_ktp_petugas.append(tuples[0])

for ktp in no_ktp_petugas:
    ktp_ktp_tuple = (ktp, ktp)
    PILIHAN_PETUGAS.append(ktp_ktp_tuple)


# Ngambil semua (id, nama) dr stasiun
query_id_nama_stasiun = "SELECT id_stasiun, nama FROM STASIUN;"
cursor.execute(query_id_nama_stasiun)

id_nama_stasiun_tuple = cursor.fetchall()
id_nama_stasiun = []
PILIHAN_STASIUN = []


for tuples in id_nama_stasiun_tuple:
    id_stasiun = tuples[0]
    nama_stasiun = tuples[1]
    id_nama_stasiun.append(id_stasiun + " - " + nama_stasiun)


for st in id_nama_stasiun:
    id_nama_id_nama = (st, st)
    PILIHAN_STASIUN.append(id_nama_id_nama)


class FormPenugasan(forms.Form):
    date_attrs = {
        'type': 'date'
    }

    no_ktp = forms.ChoiceField(
        label='No KTP petugas:',
        choices=PILIHAN_PETUGAS
    )

    # ! DI DB TIMESTAMP (DATE+TIME) TAPI DI WEB MSH DATE DOANG
    tanggal_mulai = forms.DateField(
        label='Tanggal mulai:',
        required=True,
        widget=forms.DateInput(attrs=date_attrs))

    tanggal_selesai = forms.DateField(
        label='Tanggal selesai',
        required=True,
        widget=forms.DateInput(attrs=date_attrs))

    stasiun = forms.ChoiceField(
        choices=PILIHAN_STASIUN
    )
