from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'penugasan'
urlpatterns = [
    path('update', views.update_penugasan, name='penugasan'),
    path('create', views.create_penugasan, name='penugasan'),
    path('', views.list_penugasan, name='penugasan'),
    #url(r'^$', views.delete_penugasan, name='delete_penugasan'),
    url(r'^delete/(?P<ktp>\d+)/(?P<tgl_mulai>\d+)/(?P<id_st>\d+)$', views.delete_penugasan, name='delete_penugasan'),
    url(r'^update/(?P<ktp>\d+)/(?P<tgl_mulai>\d+)/(?P<id_st>\d+)$', views.update_penugasan, name='update_penugasan'),
    #url(r'^delete/(?P<ktp>\d+)/(?P<tgl_mulai>\d+)/(?P<id_st>\d+)$', views.delete_penugasan, name='delete_penugasan')
    url(r'^', views.list_penugasan, name='penugasan'),
]