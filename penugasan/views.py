from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.db import connection
from .forms import FormPenugasan


def execute_query(query):
    with connection.cursor() as cursor:
        try:
            cursor.execute(query)
            hasil = cursor.fetchall()
            print("HASIL: ")
            print(hasil)
            connection.close()
            cursor.close()
            return hasil
        except Exception:  # insert delete update kan ga return apa2
            return


def create_penugasan(request):
    form = FormPenugasan(request.POST or None)
    response = {}

    if request.method == 'POST' and form.is_valid():

        no_ktp = request.POST.get('no_ktp')
        tanggal_mulai = request.POST.get('tanggal_mulai')
        id_stasiun = request.POST.get('stasiun').split(" - ")[0]
        tanggal_selesai = request.POST.get('tanggal_selesai')


        sql_query = "INSERT INTO PENUGASAN VALUES('"
        sql_query += no_ktp + "', '"
        sql_query += tanggal_mulai + "', '"
        sql_query += id_stasiun + "', '"
        sql_query += tanggal_selesai + "')"

        execute_query(sql_query)
        return redirect('/penugasan/')

    response = {
        "form": form,
    }
    return render(request, 'penugasan_create.html', response)


def update_penugasan(request, ktp, tgl_mulai, id_st):
    # 01234567890123
    # yyyymmddHHMMss
    #            12345678
    print("TGL MULAI: ")
    print(tgl_mulai)
    print("ID ST:")
    print(id_st)
    tgl_mulai_separated = tgl_mulai[0:4] + "-" + tgl_mulai[4:6] + "-" + tgl_mulai[6:8] + " "
    tgl_mulai_separated += tgl_mulai[8:10] + ":" + tgl_mulai[10:12] + ":" + tgl_mulai[12:]
    print("TGL MULAI SEPARATED:")
    print(tgl_mulai_separated)
    query_data_row_yg_mau_diupdate = "SELECT * FROM PENUGASAN WHERE ktp = '"
    query_data_row_yg_mau_diupdate += ktp + "' AND start_datetime = '"
    query_data_row_yg_mau_diupdate += tgl_mulai_separated + "' AND id_stasiun = '"
    query_data_row_yg_mau_diupdate += id_st + "';"

    print("QUERY DATA YG MAU DIUPDATE:")
    print(query_data_row_yg_mau_diupdate)
    data_row_yg_mau_diupdate = execute_query(query_data_row_yg_mau_diupdate)[0]

    print("DATA ROW YG MAU DIUPDATE: ")
    print(data_row_yg_mau_diupdate)
    # print("DATA ROW YG MO DIUPDATE: " + str(data_row_yg_mau_diupdate))
    # form = FormPenugasan(request.POST or None)
    attributes = {
        "no_ktp": data_row_yg_mau_diupdate[0],
        "tanggal_mulai": data_row_yg_mau_diupdate[1],
        "id_stasiun": data_row_yg_mau_diupdate[2],
        "tanggal_selesai": data_row_yg_mau_diupdate[3]
    }

    form = FormPenugasan(initial=attributes)
    if request.method == 'POST':

        tanggal_mulai_baru = request.POST.get('tanggal_mulai')
        id_stasiun_baru = request.POST.get('stasiun').split(" - ")[0]
        tanggal_selesai_baru = request.POST.get('tanggal_selesai')

        update_query = "UPDATE PENUGASAN SET start_datetime = '"
        update_query += tanggal_mulai_baru + " 00:00:00', id_stasiun = '"
        update_query += id_stasiun_baru + "', end_datetime = '"
        update_query += tanggal_selesai_baru + " 00:00:00' WHERE ktp = '"
        update_query += ktp + "' AND start_datetime = '"
        update_query += tgl_mulai_separated + "' AND id_stasiun ='"
        update_query += id_st + "'"

        print("QUERY: " + update_query)
        execute_query(update_query)

        return redirect('/penugasan')

    attributes['form'] = form

    return render(request, 'penugasan_update.html', attributes)


def list_penugasan(request):
    sql_query = "SELECT * FROM PENUGASAN"

    penugasan_objects = execute_query(sql_query)
    penugasan_objects_baru = []

    for tuples in penugasan_objects:
        no_ktp_baru = tuples[0]
        tgl_mulai_baru = tuples[1].strftime("%Y-%m-%d %H:%M:%S")
        tgl_mulai_nospace = tuples[1].strftime("%Y%m%d%H%M%S")
        id_st_baru = tuples[2]
        tgl_selesai_baru = tuples[3].strftime("%Y-%m-%d %H:%M:%S")
        tgl_selesai_nospace = tuples[3].strftime("%Y%m%d%H%M%S")
        tuple_baru = (no_ktp_baru, tgl_mulai_baru, id_st_baru, tgl_selesai_baru, tgl_mulai_nospace)
        penugasan_objects_baru.append(tuple_baru)

    response = {
        "penugasan_objects": penugasan_objects_baru
    }
    print("PENUGASAN OBJECTS:")
    print(penugasan_objects)
    print("PENUGASAN OBJECTS BARU:")
    print(penugasan_objects_baru)
    return render(request, 'penugasan_list.html', response)


def delete_penugasan(request, ktp, tgl_mulai, id_st):
    print("KTP: {}, TGL MULAI: {}, ID_ST: {}".format(ktp, tgl_mulai, id_st))
    tgl_mulai_separated = tgl_mulai[0:4] + "-" + tgl_mulai[4:6] + "-" + tgl_mulai[6:8] + " "
    tgl_mulai_separated += tgl_mulai[8:10] + ":" + tgl_mulai[10:12] + ":" + tgl_mulai[12:]
    delete_query = "DELETE FROM PENUGASAN WHERE ktp = '" + ktp + "'"
    delete_query += " AND start_datetime = '" + tgl_mulai_separated + "' AND id_stasiun = '"
    delete_query += id_st + "';"
    print(delete_query)
    
    execute_query(delete_query)

    return redirect('/penugasan')
