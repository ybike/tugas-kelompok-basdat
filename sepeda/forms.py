from django import forms
from django.forms import ModelForm
from django.db import connection

class FormSepeda(forms.Form):
    PILIHAN = [
        ('Tersedia', 'Tersedia'),
    	('Tidak Tersedia', 'Tidak Tersedia')
    ]

    class_attrs = {
    'class':'form-control'
    }

    def buatListKartuPilihan():
        list_no_kartu = []
        query_ambil_no_kartu_anggota = "SELECT NO_KARTU, NAMA FROM ANGGOTA A, PERSON P WHERE A.KTP = P.KTP ORDER BY NAMA"

        with connection.cursor() as cursor:
            cursor.execute(query_ambil_no_kartu_anggota)
            list_no_kartu = cursor.fetchall()
        connection.close()

        no_kartu_pilihan = []
        for row in list_no_kartu:
            no_kartu_pilihan.append((row[0],row[1]))
        return no_kartu_pilihan
    
    def buatListStasiunPilihan():
        list_no_stasiun = []
        query_ambil_no_stasiun = "SELECT ID_STASIUN, NAMA FROM STASIUN ORDER BY NAMA"

        with connection.cursor() as cursor:
            cursor.execute(query_ambil_no_stasiun)
            list_no_stasiun = cursor.fetchall()
        connection.close()

        no_stasiun_pilihan = []
        for row in list_no_stasiun:
            no_stasiun_pilihan.append((row[0],row[1]))
        return no_stasiun_pilihan

    merk = forms.CharField(
    	label = 'Merk', 
    	required = True,
        widget = forms.TextInput(attrs = class_attrs)
    )

    jenis = forms.CharField(
    	label = 'Jenis', 
    	required = True,
        widget = forms.TextInput(attrs = class_attrs)
    )

    status = forms.ChoiceField(
    	choices = PILIHAN,
        required = True,
    )
    
    stasiun = forms.ChoiceField(
        choices = buatListStasiunPilihan(),
        required = True,
    )

    penyumbang = forms.ChoiceField(
        choices = buatListKartuPilihan(),
        required = True,
    )

    def __init__(self, *args, **kwargs):
         super(FormSepeda, self).__init__(*args, **kwargs)
    