from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'sepeda'
urlpatterns = [
	#path('^update', views.update_acara, name ='stasiun'),
    path('^create', views.create_sepeda, name ='sepeda'),
    path('^list', views.list_sepeda, name = 'list'),
    path('', views.list_sepeda_view, name = 'view'),
    url(r'^update/', views.update_sepeda, name='update_sepeda'),
    url(r'^delete/(?P<nomor>\d+)$', views.delete_sepeda, name='delete_sepeda'),
]