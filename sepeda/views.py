from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from django.db import connection

from .forms import FormSepeda

def create_sepeda(request):
    form = FormSepeda(request.POST or None)
    response = {}

    #ambil id dari stasiun
    nomor = 0
    query_ambil_nomor_terakhir = "SELECT NOMOR FROM SEPEDA ORDER BY NOMOR DESC LIMIT 1"

    with connection.cursor() as cursor:
        cursor.execute(query_ambil_nomor_terakhir)
        nomor_terakhir = cursor.fetchall()
    connection.close()
    
    for i in nomor_terakhir:
        if(nomor_terakhir != None):
            nomor = i[0]
            print(nomor)
 
    if request.method == 'POST' and form.is_valid():
        merk = request.POST.get('merk')
        jenis = request.POST.get('jenis')
        status = request.POST.get("status")

        if(status == 'Tersedia'):
            status = True
        else:
            status = False

        stasiun = request.POST.get('stasiun')
        penyumbang = request.POST.get('penyumbang')
      
        sql_query = "INSERT INTO SEPEDA VALUES('{}','{}','{}',{},'{}','{}')".format(
            str(int(nomor)+1),
            str(merk), 
            str(jenis), 
            status, 
            str(stasiun),
            str(penyumbang)
        )

        with connection.cursor() as cursor:
            cursor.execute(sql_query)
        connection.close()
        
        return redirect('/bike')
    
    response = {"form" : form}

    return render(request, 'sepeda_create.html', response)

def list_sepeda_view(request):
    return render(request, 'sepeda_list.html')

def list_sepeda(request):
    query_get_all = "SELECT S.NOMOR, S.MERK, S.JENIS, S.STATUS, ST.NAMA, P.NAMA " 
    query_get_all += "FROM SEPEDA S, STASIUN ST, PERSON P, ANGGOTA A "
    query_get_all += "WHERE S.ID_STASIUN = ST.ID_STASIUN AND S.NO_KARTU_PENYUMBANG = A.NO_KARTU AND A.KTP = P.KTP "
    query_get_all += "ORDER BY S.NOMOR ASC"
    
    with connection.cursor() as cursor:
        cursor.execute(query_get_all)
        sepeda_objects = cursor.fetchall()
    connection.close()

    return JsonResponse({'data' : sepeda_objects})

def delete_sepeda(request, nomor):
    delete_query = "DELETE FROM SEPEDA WHERE NOMOR = '" + nomor + "'"
    with connection.cursor() as cursor:
        cursor.execute(delete_query)
    connection.close()
    return redirect('/bike')

def update_sepeda(request):
    merk_sepeda = request.GET.get('merk')
    nomor_sepeda = request.GET.get('nomor')
    jenis_sepeda = request.GET.get('jenis')
    status_sepeda = request.GET.get('status')
    stasiun_sepeda = request.GET.get('stasiun')
    penyumbang_sepeda = request.GET.get('penyumbang')

    response = {}

    query_get_spesific_bike = "SELECT * FROM SEPEDA WHERE NOMOR = '" + nomor_sepeda + "'"
    with connection.cursor() as cursor:
        cursor.execute(query_get_spesific_bike)
        sepeda_objects = cursor.fetchall()
    connection.close()

    sepeda = sepeda_objects[0]
    station_name = ''
    penyumbang_name = ''

    query_get_name_station = "SELECT NAMA FROM STASIUN ST WHERE ID_STASIUN = '" + sepeda[4] + "'"
    query_get_name_penyumbang = "SELECT P.NAMA FROM PERSON P, ANGGOTA A WHERE A.NO_KARTU = '" + sepeda[5] + "' AND A.KTP = P.KTP"

    with connection.cursor() as cursor:
        cursor.execute(query_get_name_station)
        station_name = cursor.fetchall()[0][0]
        cursor.execute(query_get_name_penyumbang)
        penyumbang_name = cursor.fetchall()[0][0]
    connection.close()

    form = FormSepeda(initial={
        "merk": merk_sepeda, 
        "jenis": jenis_sepeda, 
        "status":(sepeda[3], status_sepeda), 
        "stasiun":(sepeda[4], stasiun_sepeda),
        "penyumbang":(sepeda[5], penyumbang_sepeda)
        }
    )

    if request.method == 'POST':
        merk = request.POST.get('merk')
        jenis = request.POST.get('jenis')
        status = request.POST.get("status")
        if(status == "Tersedia"):
            status = True
        else:
            status = False

        stasiun = request.POST.get('stasiun')
        penyumbang = request.POST.get('penyumbang')

        update_query = "UPDATE SEPEDA SET MERK = '"
        update_query += merk + "', JENIS = '"
        update_query += jenis + "', STATUS = '"
        update_query += str(status) + "', ID_STASIUN = '"
        update_query += stasiun + "', NO_KARTU_PENYUMBANG = '"
        update_query += penyumbang + "' "
        update_query += "WHERE NOMOR = '" + sepeda[0] + "';"

        print(update_query)

        with connection.cursor() as cursor:
            cursor.execute(update_query)
        connection.close()

        return redirect('/bike')

    status = ''
    if (sepeda[3]):
        status2 = "Tersedia"
    else:
        status2 = "Tidak Tersedia"

    response = {
        "form" : form
    }

    return render(request, 'sepeda_update.html', response)




