from django import forms
from django.forms import ModelForm

class_attrs = {
    'class':'form-control'
}

class FormStasiun(forms.Form):
    nama = forms.CharField(
        label="Nama Stasiun",
        max_length=50,
        required=True,
        widget=forms.TextInput(attrs=class_attrs)
    )

    alamat = forms.CharField(
        label="Alamat",
        widget=forms.Textarea(attrs=class_attrs)
    )

    latitude = forms.IntegerField(
        label="Latitude",
        max_value=99999999999999999999,
        widget=forms.NumberInput(attrs=class_attrs)
    )

    longitude = forms.IntegerField(
        label="Longitude",
        max_value=99999999999999999999,
        widget=forms.NumberInput(attrs=class_attrs)
    )

    def __init__(self, *args, **kwargs):
         super(FormStasiun, self).__init__(*args, **kwargs)


    

