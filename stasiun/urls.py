from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'stasiun'
urlpatterns = [
    path('^create', views.create_stasiun, name ='stasiun'),
    path('', views.list_stasiun_view, name = 'view'),
    path('^list', views.list_stasiun, name= 'list'),
    url(r'^update/', views.update_stasiun, name='update_stasiun'),
    url(r'^delete/(?P<id_stasiun>\d+)$', views.delete_stasiun, name='delete_stasiun'),
]