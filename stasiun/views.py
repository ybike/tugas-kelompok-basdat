from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse, JsonResponse
from django.db import connection

from .forms import FormStasiun

def create_stasiun(request):
    form = FormStasiun(request.POST or None)
    response = {}

    #ambil id dari stasiun
    id = 0
    query_ambil_id_terakhir = "SELECT ID_STASIUN FROM STASIUN ORDER BY ID_STASIUN DESC LIMIT 1"

    with connection.cursor() as cursor:
        cursor.execute(query_ambil_id_terakhir)
        id_terakhir = cursor.fetchall()
    connection.close()

    for i in id_terakhir:
        if(id_terakhir != None):
            id = i[0]
    
    if request.method == 'POST' and form.is_valid():
        nama = request.POST.get('nama')
        alamat = request.POST.get('alamat')
        latitude = request.POST.get('latitude')
        longitude = request.POST.get('longitude')
      
        sql_query = "INSERT INTO STASIUN VALUES('{}','{}',{},{},'{}')".format(
            str(int(id)+1), 
            alamat, 
            latitude, 
            longitude,
            nama
            )
        with connection.cursor() as cursor:
            cursor.execute(sql_query)
        connection.close()

        return redirect('/stasiun')
    
    response = {"form" : form}

    return render(request, 'stasiun_create.html', response)

def list_stasiun_view(request):
    return render(request, 'stasiun_list.html')

def list_stasiun(request):
    query_get_all = "SELECT * FROM STASIUN ORDER BY ID_STASIUN ASC"

    with connection.cursor() as cursor:
        cursor.execute(query_get_all)
        stasiun_objects = cursor.fetchall()
    connection.close()

    return JsonResponse({'data' : stasiun_objects})

def delete_stasiun(request,id_stasiun):
    delete_query = "DELETE FROM STASIUN WHERE ID_STASIUN = '" + id_stasiun + "'"
    with connection.cursor() as cursor:
        cursor.execute(delete_query)
    connection.close()
    return redirect('/stasiun')

def update_stasiun(request):
    id_stasiun = request.GET.get('id')
    nama_stasiun = request.GET.get('nama')
    alamat_stasiun = request.GET.get('alamat')

    form = FormStasiun(initial={"alamat": alamat_stasiun, "nama": nama_stasiun})
    response = {}

    query_get_spesific_station = "SELECT * FROM STASIUN WHERE ID_STASIUN = '" + id_stasiun + "'"
    with connection.cursor() as cursor:
        cursor.execute(query_get_spesific_station)
        station_objects = cursor.fetchall()
    connection.close()

    stasiun = station_objects[0]    

    if request.method == 'POST':
        nama = request.POST.get('nama')
        alamat = request.POST.get('alamat')
        latitude = request.POST.get('latitude')
        longitude = request.POST.get('longitude')

        update_query = "UPDATE STASIUN SET NAMA = '"
        update_query += nama + "', ALAMAT = '"
        update_query += alamat + "', LAT = "
        update_query += latitude + ", LONG = "
        update_query += longitude + " "
        update_query += "WHERE ID_STASIUN = '" + stasiun[0] + "';"

        print(update_query)

        with connection.cursor() as cursor:
            cursor.execute(update_query)
        connection.close()

        return redirect('/stasiun')

    response = {
        "form" : form,
        "lat" : stasiun[2],
        "long" : stasiun[3],
    }

    return render(request, 'stasiun_update.html', response)
