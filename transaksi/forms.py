from django import forms
from django.forms import ModelForm
from . import models

class_attrs = {
    'class':'form-control'
}

class TopUpForm(forms.Form):
    nominal = forms.FloatField(
        label="Nominal",
        required=True,
        widget=forms.NumberInput(attrs=class_attrs)
    )
