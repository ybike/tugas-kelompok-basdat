from django.db import models

# Create your models here.
class Transaksi(models.Model):
    TIPE_TRANSAKSI = (
        ('PJ', "Peminjaman"),
        ('TU', "Top-up")
        )
    jenis = models.CharField(choices = TIPE_TRANSAKSI, max_length=2)
    tanggal = models.DateField(auto_now_add=True)
    nominal = models.FloatField()
    
