from django.urls import path
from . import views

app_name = "transaksi"
urlpatterns = [
    path('topUp/',views.topUp,name ='topUp'),
    path('',views.show_transaksi,name ='history'),
    
]
