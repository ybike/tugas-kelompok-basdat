from django.shortcuts import render, redirect
from django.db import connection
import datetime
from .forms import TopUpForm
from .models import Transaksi

# Create your views here.
def use_cursor(operation, query):
    result = None
    with connection.cursor() as cursor:
        cursor.execute(query)
        
        if operation == 'getAll':
            result = cursor.fetchall()
        elif operation == 'getOne':
            result = cursor.fetchone()
        else:
            pass
    return result

def topUp(request):
    form = TopUpForm(request.POST or None)
    response = {}
    if request.method == 'POST' and form.is_valid():
        nominal = request.POST.get('nominal')
        if(int(nominal) < 0 ): response = { 'invalid' : 'Invalid nominal'}
        else :
            date_time = datetime.datetime.now()
            ktp = request.session['user'][0]
            no_anggota = use_cursor('getOne', 'SELECT no_kartu FROM ANGGOTA'+
                                    " WHERE ktp = '"+ktp+"'")[0]

            use_cursor('add', 'INSERT INTO TRANSAKSI VALUES('+
                       no_anggota+", '"+str(date_time)+"', 'Top Up', "+nominal+')')
            use_cursor('update', "UPDATE ANGGOTA SET saldo = saldo+"+nominal+
                       " WHERE ktp = '"+ktp+"'")
            return redirect('/transaksi/')
    
    response["formTopUp"] = form
    return render(request,"topUp.html",response)

def show_transaksi (request):
    ktp = request.session['user'][0]
    no_anggota = use_cursor('getOne', 'SELECT no_kartu FROM ANGGOTA'+
                                " WHERE ktp = '"+ktp+"'")[0]
    query = "SELECT * FROM TRANSAKSI WHERE no_kartu_anggota = '"+no_anggota+"'"
    rawTransaksis = use_cursor('getAll', query)
    
    nomor = 0
    transaksis = []
    for transaksi in rawTransaksis:
        nomor += 1
        transaksi += (nomor,)
        transaksis.append(transaksi)
    
    response = {"transaksis" : transaksis}
    return render(request,"transaksi_show.html", response)
