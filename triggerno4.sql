CREATE FUNCTION emp_stamp() RETURNS trigger AS $emp_stamp$
    BEGIN
        -- Check that empname and salary are given
        IF NEW.empname IS NULL THEN
            RAISE EXCEPTION 'empname cannot be null';
        END IF;
        IF NEW.salary IS NULL THEN
            RAISE EXCEPTION '% cannot have null salary', NEW.empname;
        END IF;

        -- Who works for us when she must pay for it?
        IF NEW.salary < 0 THEN
            RAISE EXCEPTION '% cannot have a negative salary', NEW.empname;
        END IF;

        -- Remember who changed the payroll when
        NEW.last_date := current_timestamp;
        NEW.last_user := current_user;
        RETURN NEW;
    END;
$emp_stamp$ LANGUAGE plpgsql;
















CREATE OR REPLACE FUNCTION new_row_di_tkp_dan_transaksi() 
    RETURNS trigger as $$
DECLARE
    jumlah_nominal    INTEGER;
BEGIN
    IF (TG_OP = 'INSERT') THEN
    --IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
        IF NEW.datetime_kembali IS NOT NULL THEN
            jumlah_nominal := NEW.biaya + NEW.denda;
            INSERT INTO TRANSAKSI(
                no_kartu_anggota, 
                date_time, 
                jenis, 
                nominal
            )
            VALUES(NEW.no_kartu_anggota, 
                NEW.datetime_kembali, 
                'Peminjaman', 
                jumlah_nominal
            );

            INSERT INTO TRANSAKSI_KHUSUS_PEMINJAMAN(
                no_kartu_anggota,
                date_time,
                datetime_pinjam,
                no_sepeda,
                id_stasiun,
                no_kartu_peminjam
            )
            VALUES(NEW.no_kartu_anggota, 
                NEW.datetime_kembali, 
                NEW.datetime_pinjam,
                NEW.nomor_sepeda, 
                NEW.id_stasiun, 
                NEW.no_kartu_anggota);
            
        END IF;
        RETURN NEW;
    --ELSIF (TG_OP = 'UPDATE')
END;
$$
LANGUAGE plpgsql;


CREATE TRIGGER new_row_di_tkp_dan_transaksi_trigger
  AFTER INSERT
  ON PEMINJAMAN
  FOR EACH ROW
  EXECUTE PROCEDURE new_row_di_tkp_dan_transaksi();

INSERT INTO PEMINJAMAN VALUES (
    '5974107696', 
    '2019-06-20 10:00:00', 
    '2019-06-20 14:00:00',
    30.00,
    30.00, 
    '614313257',
    '6'
);






CREATE OR REPLACE FUNCTION add_to_laporan()
RETURNS trigger AS
$$
    DECLARE
        temp_row RECORD;
    BEGIN
        IF (TG_OP = 'INSERT' OR TG_OP = 'UPDATE') THEN
            raise notice 'Value: %', random();
            FOR temp_row IN
            SELECT no_kartu_anggota as a, datetime_pinjam as b, nomor_sepeda as c, id_stasiun as d
            FROM PEMINJAMAN
            GROUP BY no_kartu_anggota, datetime_pinjam, datetime_kembali, id_stasiun, nomor_sepeda
            HAVING DATE_PART('day', datetime_kembali - datetime_pinjam) > 1
            LOOP
            INSERT INTO 
            LAPORAN(id_laporan, no_kartu_anggota, datetime_pinjam, nomor_sepeda, id_stasiun, status)
            VALUES (floor(1000000 * random()), temp_row.a, temp_row.b, temp_row.c, temp_row.d, floor(1000000 * random()));
            END LOOP;
        END IF;
        RETURN NEW;
    END;
$$
LANGUAGE plpgsql;



tkp = 45
laporan = 15
peminjaman = 55

















INSERT INTO PEMINJAMAN VALUES (
    '11111',
    '2019-06-20 10:00:00', 
    '2019-06-20 14:00:00',
    30.00,
    30.00, 
    '614313257',
    '6',
);