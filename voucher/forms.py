from django import forms
from django.db import connection

cursor = connection.cursor()

query_no_kartu = "SELECT no_kartu FROM ANGGOTA;"
cursor.execute(query_no_kartu)

no_kartu_tuple = cursor.fetchall()
no_kartu_arr = []
PILIHAN_NO_KARTU= []

for tuples in no_kartu_tuple:
    no_kartu_anggota = tuples[0]
    no_kartu_arr.append(no_kartu_anggota)

for nk in no_kartu_arr:
    kartu = (nk, nk)
    PILIHAN_NO_KARTU.append(kartu)

class FormVoucher(forms.Form):
    color_attribute = {
        'style': 'color:black;'
    }

    nama = forms.CharField(
        label='Nama',
        required=True,
        widget=forms.TextInput(attrs=color_attribute)
    )

    kategori = forms.CharField(
        label='Kategori',
        required=True,
        widget=forms.TextInput(attrs=color_attribute)
    )

    poin = forms.IntegerField(
        label='Poin',
        required=True,
        widget=forms.TextInput(attrs=color_attribute)
    )

    deskripsi = forms.CharField(
        label='Deskripsi',
        required=True,
        widget=forms.TextInput(attrs=color_attribute)
    )

    jumlah = forms.IntegerField(
        label='Jumlah',
        required=True,
        widget=forms.TextInput(attrs=color_attribute)
    )

    no_kartu = forms.ChoiceField(
        required=True,
        choices=PILIHAN_NO_KARTU
    )

    def __init__(self, *args, **kwargs):
         super(FormVoucher, self).__init__(*args, **kwargs)

   
