# Generated by Django 2.1.1 on 2019-04-29 09:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voucher', '0003_auto_20190427_1429'),
    ]

    operations = [
        migrations.CreateModel(
            name='voucher_update',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=50)),
                ('kategori', models.CharField(max_length=50)),
                ('poin', models.CharField(max_length=50)),
                ('deskripsi', models.CharField(max_length=100)),
                ('jumlah', models.CharField(max_length=50)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.RenameModel(
            old_name='voucher',
            new_name='voucher_create',
        ),
    ]
