# from django.urls import path
# from . import views

# app_name = "voucher"
# urlpatterns = [
#     path('^create/',views.create_voucher,name ='voucher'),
#     path('^update/',views.update_voucher,name ='voucher'),
#     path('',views.show_voucher,name ='voucher'),
    
# ]

from django.urls import path
from . import views
from django.conf.urls import url

app_name = 'voucher'
urlpatterns = [
    path('update', views.update_voucher, name='update_voucher'),
    path('create', views.create_voucher, name='create_voucher'),
    path('delete', views.delete_voucher, name='delete_voucher'),
    path('update/', views.update_voucher, name='update_voucher'),
    path('create/', views.create_voucher, name='create_voucher'),
    path('delete/', views.delete_voucher, name='delete_voucher'),
    path('', views.list_voucher, name='voucher'),
    url(r'^delete/(?P<voucher_id>\d+)$', views.delete_voucher, name='delete_voucher'),
    url(r'^update/(?P<voucher_id>\d+)$', views.update_voucher, name='update_voucher')
]
