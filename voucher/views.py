from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.db import connection
from .forms import FormVoucher


def execute_query(query):
    with connection.cursor() as cursor:
        try:
            cursor.execute(query)
            hasil = cursor.fetchall()
            print("HASIL: ")
            print(hasil)
            connection.close()
            cursor.close()
            return hasil
        except Exception:  # insert delete update kan ga return apa2
            return

        #if hasil is None or hasil == None or hasil == '' or hasil == []:
        #    return
        #else:
        #    return hasil

# updatenya ga kepikiran caranya preloaded :( susah banget dah


def list_voucher(request):

    sql_query = "SELECT * FROM VOUCHER"

    voucher_objects = execute_query(sql_query)

    response = {
        "voucher_objects": voucher_objects
    }

    return render(request, 'voucher_show.html', response)


def create_voucher(request):
    form = FormVoucher(request.POST or None)
    response = {}

    query_get_all = "SELECT id_voucher FROM VOUCHER"
    #cursor.execute(query_get_all)
    list_id_kepake_masih_tuple = execute_query(query_get_all)  # ambil idnya tp msh tuple

    list_id_kepake = []                             # dah ga tuple lg
    for tuples in list_id_kepake_masih_tuple:
        list_id_kepake.append(tuples[0])

    id_terakhir = int(list_id_kepake[len(list_id_kepake) - 1])

    if request.method == 'POST' and form.is_valid():
        nama = request.POST.get('nama')
        kategori = request.POST.get('kategori')
        poin = request.POST.get('poin')
        deskripsi = request.POST.get('deskripsi')
        jumlah = int(request.POST.get('jumlah'))
        no_kartu = request.POST.get('no_kartu')

        # get id terakhir trus diincrement
        id_terakhir = int(list_id_kepake[len(list_id_kepake) - 1])
        id_voucher = id_terakhir + 1

        """
        while str(id_voucher) in list_id_kepake:
            id_voucher += 1

        sql_query = "INSERT INTO VOUCHER VALUES("
        sql_query += "'" + str(id_voucher) + "',"
        sql_query += "'" + nama + "',"
        sql_query += "'" + kategori + "',"
        sql_query += "'" + poin + "',"
        sql_query += "'" + deskripsi + "',"
        # sql_query += "'" + jumlah + "')"
        sql_query += "'" + no_kartu + "')"
        """
        #execute_query(sql_query)
        #print(sql_query)

        for i in range(int(jumlah)):
            while id_voucher in list_id_kepake:
                id_voucher += 1

            sql_query = "INSERT INTO VOUCHER VALUES("
            sql_query += "'" + str(id_voucher) + "',"
            sql_query += "'" + nama + "',"
            sql_query += "'" + kategori + "',"
            sql_query += "'" + poin + "',"
            sql_query += "'" + deskripsi + "',"
            #sql_query += "'" + jumlah + "')"
            sql_query += "'" + no_kartu + "')"
            execute_query(sql_query)
            id_voucher += i

        print(sql_query)

        response = {
            "form": form
        }

        return redirect('/voucher/')

    response = {"form": form}

    return render(request, 'voucher_create.html', response)


def update_voucher(request, voucher_id):

    query_data_row_yg_mau_diupdate = "SELECT * FROM VOUCHER WHERE id_voucher = '"
    query_data_row_yg_mau_diupdate += voucher_id + "';"

    data_row_yg_mau_diupdate = execute_query(query_data_row_yg_mau_diupdate)[0]

    attributes = {
        # 'form': form,
        # 'id_voucher': data_row_yg_mau_diupdate[0],
        'nama': data_row_yg_mau_diupdate[1],
        'kategori': data_row_yg_mau_diupdate[2],
        'poin': data_row_yg_mau_diupdate[3],
        'deskripsi': data_row_yg_mau_diupdate[4],
        'jumlah': data_row_yg_mau_diupdate[5]
    }

    # form = FormVoucher(request.POST or None)
    form = FormVoucher(initial=attributes)

    if request.method == 'POST':
        nama_baru = request.POST.get('nama')
        kategori_baru = request.POST.get('kategori')
        poin_baru = request.POST.get('poin')
        deskripsi_baru = request.POST.get('deskripsi')

        update_query = "UPDATE VOUCHER SET nama = '"
        update_query += nama_baru + "', kategori = '"
        update_query += kategori_baru + "', nilai_poin = '"
        update_query += poin_baru + "', deskripsi = '"
        update_query += deskripsi_baru + "'"
        update_query += "WHERE id_voucher = '" + voucher_id + "';"

        print(update_query)
        execute_query(update_query)

        return redirect('/voucher/')

    attributes['form'] = form
    return render(request, 'voucher_update.html', attributes)


def delete_voucher(request, voucher_id):
    delete_query = "DELETE FROM VOUCHER WHERE id_voucher = '" + voucher_id + "'"
    with connection.cursor() as cursor:
        cursor.execute(delete_query)
    connection.close()

    return redirect('/voucher/')
